package com.bank.abc.service;

import com.bank.abc.entity.AccountEntity;
import com.bank.abc.exception.AccountNotFoundException;
import com.bank.abc.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class AccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Transactional
    public AccountEntity createAccount(AccountEntity accountEntity) {
        return accountRepository.save(accountEntity);
    }

    @Transactional
    public AccountEntity updateAccount(AccountEntity accountEntity, int id) {
        AccountEntity accountDetails = this.findAccountById(id);
        if (accountDetails != null) {
            accountDetails.setAccountBalance(accountEntity.getAccountBalance());
            accountDetails.setAccountType(accountEntity.getAccountType());
            accountDetails.setHolderName(accountEntity.getHolderName());
            return accountRepository.save(accountDetails);
        }
        return new AccountEntity();
    }

    @Transactional
    public Integer updateAccountBalance(AccountEntity accountEntity, int id) {
        this.findAccountById(id);
        accountRepository.updateAccountBalance(accountEntity.getAccountBalance(), id);
        return 1;
    }

    public AccountEntity findAccountById(int id) {
        // naming query
//        accountRepository.findByIdAndDeletedFalse(id);
//        List<AccountEntity> list = accountRepository.findByAccountBalanceInAndDeletedFalse(Arrays.asList("5"));
        //return accountRepository.findById(id).orElseThrow(() -> new AccountNotFound(id));
        return accountRepository.findById(id).orElseThrow(() ->
                new AccountNotFoundException("Account details not found for id: " + id));
    }

    public List<AccountEntity> findAllAccounts() {
        return accountRepository.findAll();
    }

    public String deleteAccountById(int id) {
        AccountEntity accountEntity = this.findAccountById(id);
        accountRepository.deleteById(id);
        return "Account with id: " + id + " has been deleted";
    }
}
