package com.bank.abc.service;

import com.bank.abc.controller.AccountController;
import com.bank.abc.entity.AccountEntity;
import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

public class AccountServiceTest {

    @Mock
    public int accountId;

    @Mock
    public AccountEntity accountEntity;

    @Mock
    public AccountController accountController;

    @Mock
    private MockMvc mockMvc;

    @Autowired
    WebApplicationContext webApplicationContext;

    @Mock
    private AccountService accountService;

    @Test
    public void testCreateAccount() throws IOException, JSONException {
        Map<String, String> map = new HashMap<>();
        String uri = "accounts";
        String requestBody = "{\n" +
                "    \"holderName\":\"Adarsh\",\n" +
                "    \"accountType\":\"saving\",\n" +
                "    \"accountBalance\":120023\n" +
                "}";

        try {
            MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post(uri)
                    .contentType(MediaType
                            .APPLICATION_JSON)
                    .content(requestBody)).andReturn();
            int status = mvcResult.getResponse().getStatus();
            assertEquals(200, status);
            String content = mvcResult.getResponse().getContentAsString();
            assertTrue(content.contains("successfully account created"));
            JacksonJsonParser parser = new JacksonJsonParser();
            Map<String, Object> mapObject = new HashMap<>();
            parser.parseMap(content).get("holderName");

        } catch (Exception exception) {
            System.out.println("assertionError: " + exception);
        }
    }

    @Test
    public void testFindById() throws IOException, JSONException {
        when(accountService.findAccountById(accountId)).thenReturn(accountEntity);
        try {
            assertEquals(accountController.findAccountById(accountId), accountEntity);
        } catch (Exception exception) {
            System.out.println("assertionError: " + exception);
        }
    }


//    @Test
//    @DisplayName("test should pass when an account is created")
//    void createAccount() {
//        AccountEntity accountEntity = new AccountEntity();
//        accountEntity.setHolderName("Adarsh");
//        accountEntity.setAccountType("saving");
//        accountEntity.setAccountBalance(10000);
//
//        AccountService accountService = new AccountService();
//        assertEquals("Successfully added a category inside the table", true, accountService.createAccount(accountEntity).toString());
//    }
//
//    @Test
//    @DisplayName("test should pass when an account is created")
//    void testfindAccountById() {
//        AccountService service = new AccountService();
//        AccountEntity entity = service.findAccountById(1);
//
//        assertEquals("Successfully fetched accounts detail form the table !", "Adarsh", entity.getHolderName());
//
//        //Assertions.assertFalse();
//    }
}