package com.bank.abc.controller;

import com.bank.abc.entity.AccountEntity;
import com.bank.abc.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/app/v1/accounts")
public class AccountController {

    @Autowired
    private AccountService accountService;

    @PostMapping
    public AccountEntity createAccount(@RequestBody AccountEntity accountEntity) {
        return accountService.createAccount(accountEntity);
    }

    // localhost:8080/v1/accounts/2
    @PutMapping("/{id}")
    public AccountEntity updateAccount(@RequestBody AccountEntity accountEntity, @PathVariable int id) {
        return accountService.updateAccount(accountEntity, id);
    }

//    @PutMapping("/accounts/balance")
//    public Integer updateAccountBalance(@RequestBody AccountEntity accountEntity) {
//        accountService.updateAccountBalance(accountEntity);
//        return 1;
//    }

    // update account details partially
    @PatchMapping("/{id}")
    public ResponseEntity<?> updateBalance(@RequestBody AccountEntity accountEntity, @PathVariable int id) {
        accountService.updateAccountBalance(accountEntity, id);
        return new ResponseEntity("Account id: " + id + " has been updated with balance " +
                accountEntity.getAccountBalance(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public AccountEntity findAccountById(@PathVariable int id) {
        return accountService.findAccountById(id);
    }

    @GetMapping
    public List<AccountEntity> findAllAccounts() {
        return accountService.findAllAccounts();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteAccountsById(@PathVariable int id) {
        String response = accountService.deleteAccountById(id);
        //return ResponseEntity.ok().build();
        return new ResponseEntity(response, HttpStatus.OK);
    }
}
