package com.bank.abc.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;

@ControllerAdvice
public class GlobalExceptionHandler {

    // handle specific exceptions
    @ExceptionHandler(AccountNotFoundException.class)
    public ResponseEntity<?> handleAccountNotFoundException(AccountNotFoundException exception, WebRequest request) {
        AccountErrorDetails accountErrorDetails =
                new AccountErrorDetails(new Date(), exception.getMessage(), request.getDescription(false));
        return new ResponseEntity(accountErrorDetails, HttpStatus.NOT_FOUND);
    }

    // handle global exception
    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> handlerGlobalException(Exception exception, WebRequest request) {
        AccountErrorDetails accountErrorDetails =
                new AccountErrorDetails(new Date(), exception.getMessage(), request.getDescription(false));
        return new ResponseEntity(accountErrorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
