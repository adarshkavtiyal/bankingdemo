package com.bank.abc.repository;

import com.bank.abc.entity.AccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends JpaRepository<AccountEntity, Integer> {

//    @Query(value = "update ACCOUNT_ENTITY set ACCOUNT_BALANCE=:account_balance where ACCOUNT_ID =:id", nativeQuery = true)

    // JPQL
    @Modifying
    @Query("update AccountEntity set accountBalance=:account_balance where id=:id")
    public Integer updateAccountBalance(@Param("account_balance") int accountBalance, @Param("id") int id);

//    void findByIdAndDeletedFalse(int id);
//    List<AccountEntity> findByAccountBalanceInAndDeletedFalse(List<String> asList);
}
